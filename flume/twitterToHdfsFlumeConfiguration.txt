#Naming the components on the current agent. 
TwitterAgent.sources = Twitter
TwitterAgent.channels = MemChannel
TwitterAgent.sinks = HDFS

# Describing/Configuring the source 
#TwitterAgent.sources.Twitter.type = com.cloudera.flume.source.TwitterSource
TwitterAgent.sources.Twitter.type = org.apache.flume.source.twitter.TwitterSource
TwitterAgent.sources.Twitter.consumerKey = consumerKey
TwitterAgent.sources.Twitter.consumerSecret= consumerSecret
TwitterAgent.sources.Twitter.accessToken= accessToken
TwitterAgent.sources.Twitter.accessTokenSecret= accessTokenSecret
#TwitterAgent.sources.Twitter.keywords=hadoop, big data,bigdaa,cloudera,hive
TwitterAgent.sources.Twitter.keywords= McChicken, Mr. Fuji, #VMAs, Twitter, tweets,2016,usa, cloudera, bigdata, hadoop, spark, scala

# Describing/Configuring the sink 

TwitterAgent.sink.HDFS.type = hdfs
TwitterAgent.sink.HDFS.hdfs.path = /user/cloudera/twitter/tweets/%y-%m-%d
TwitterAgent.sink.HDFS.hdfs.filtType = DataStream
TwitterAgent.sink.HDFS.hdfs.writeFormat = Text
TwitterAgent.sink.HDFS.hdfs.batchSize= 10
TwitterAgent.sink.HDFS.hdfs.rollSize=0
TwitterAgent.sink.HDFS.hdfs.rollCount=100

# Describing/Configuring the channel 
TwitterAgent.channels.MemChannel.type = memory
TwitterAgent.channels.MemChannel.capacity=1000
TwitterAgent.channels.MemChannel.transactionCapacity=10

# Binding the source and sink to the channel 
TwitterAgent.sources.Twitter.channels = MemChannel
TwitterAgent.sinks.HDFS.channel = MemChannel 


