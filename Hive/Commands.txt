To show All tables 
-------------------
show tables

To show All databases
-------------------
show databases;

To use a specific database
-------------------
use <dataBaseName>

To execute sqls from script file with -f File option
hive -f <scriptFileName>
hive -f insertQueries.sql

To execute Sql 
hive -e <sqlQuery> 
hive -e 'SELEECT * FROM dummy'
Note: Need not to mention ; if we use -e option 
