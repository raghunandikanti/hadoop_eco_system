#To check whether a file exists in HDFS or not
#======================================
v_FileName="/user/xyz/input.txt"

hadoop fs -test -s ${v_FileName}
if [ $? -ne 0 ]; then
    echo "File : \"${v_FileName}\" does not exists."
    exit 1
else 
    echo "File:\"${v_FileName}\" does exists"
fi

#Content of a file
#=======================================
v_MetaData=`hadoop fs -cat ${v_FileName}`

while read -r Line
do
     echo ${Line}
done < < (echo ${v_MetaData})

#Delete a file
#=============
hadoop fs -rm -f -r "${v_FileName}"

#Append a File
===============
hadoop fs -appendToFile <Local_File> <Destination_File>
Ex:
hadoop fs -appendToFile File1.txt /user/gdevaraj/cca175/HDFS/File2.txt

echo "Content" | hadoop fs -appendToFile - <Destination_File>
Ex:
 echo -e "Line11\nLine12\nLine13" | hadoop fs -appendToFile - <Destination_File>



 
