--To view the Hadoop JOB List

hadoop job -list (deprecated)
hadoop job -list all (deprecated)

mapred job -list (Mapred Command)
mapred job -list all (Mapred Command)

--To kill a Hadoop JOB 

hadoop job -kill <JOB_ID>
Ex: 
hadoop  job -kill job_1454776203479_3725
mapred job -kill  job_1454776203479_3725

