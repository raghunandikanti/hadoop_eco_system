#To validate  a directory exists or not
#==================
hadoop fs -test -e ${v_directory_name}
if [ $? -eq 0]; then
   echo "Directory : ${v_directory_name} exists"
   hadoop fs -rm -R ${v_directory_name}
fi

#To create a directory
#================== 
hadoop fs -mkdir ${v_directory_name}