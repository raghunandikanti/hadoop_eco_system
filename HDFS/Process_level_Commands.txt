To verify whether Resource Manager and Node Manager are running
=========================================================
ps -ef|grep -i manager

To verify NameNode and Secondary NameNode
=========================================================
ps -fu hdfs


To return Resource Manager
=========================================================
ps -fu yarn