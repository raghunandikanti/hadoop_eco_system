mysql Login in Linux

mysql -u root -p
Enter password:

mysql> create database payrolls;
Query OK, 1 row affected (0.00 sec)

mysql> use payrolls;
Database changed
mysql> create table employee(empid INT,empname VARCHAR(25),empage INT);
Query OK, 0 rows affected (0.01 sec)

mysql> insert into employee VALUES(1000,'Alfred',30);
Query OK, 1 row affected (0.00 sec)

mysql> insert into employee VALUES(1001,'Albert',30);
Query OK, 1 row affected (0.00 sec)

mysql> insert into employee VALUES(1002,'Alfie',31);
Query OK, 1 row affected (0.01 sec)

mysql> insert into employee VALUES(1003,'Julie',31);
Query OK, 1 row affected (0.01 sec)

mysql> insert into employee VALUES(1004,'Jansie',31);
Query OK, 1 row affected (0.00 sec)

mysql> insert into employee VALUES(1005,'Jeniffer',31);
Query OK, 1 row affected (0.00 sec)

mysql> insert into employee VALUES(1006,'Jillan',31);
Query OK, 1 row affected (0.00 sec)

mysql> insert into employee VALUES(1007,'Eric',31);
Query OK, 1 row affected (0.01 sec)

mysql> insert into employee VALUES(1008,'Edward',32);
Query OK, 1 row affected (0.00 sec)

mysql> insert into employee VALUES(1009,'Eswar',33);
Query OK, 1 row affected (0.00 sec)

mysql> insert into employee VALUES(1010,'Shiva',33);
Query OK, 1 row affected (0.00 sec)

mysql> select * from employee;
+-------+----------+--------+
| empid | empname  | empage |
+-------+----------+--------+
|  1000 | Alfred   |     30 |
|  1001 | Albert   |     30 |
|  1002 | Alfie    |     31 |
|  1003 | Julie    |     31 |
|  1004 | Jansie   |     31 |
|  1005 | Jeniffer |     31 |
|  1006 | Jillan   |     31 |
|  1007 | Eric     |     31 |
|  1008 | Edward   |     32 |
|  1009 | Eswar    |     33 |
|  1010 | Shiva    |     33 |
+-------+----------+--------+
11 rows in set (0.00 sec)

--Grant privilege to the User :Cloudera
grant all privileges on payrolls.* To 'cloudera'@'%' with Grant Option;

create table employee_copy(empid INT,empname VARCHAR(25),empage INT);








