HBase Architecture

Reference : http://hbase.apache.org/0.94/book/book.html

ROOT ????
META ????
MEMSTORE ????
STORE ????

Regions
Regions are the basic element of availability and distribution for tables, and are comprised of a Store per Column Family. The hierarchy of objects is as follows:

Table       (HBase table)
    Region       (Regions for the table)
         Store          (Store per ColumnFamily for each Region for the table)
              MemStore           (MemStore for each Store for each Region for the table)
              StoreFile          (StoreFiles for each Store for each Region for the table)
                    Block             (Blocks within a StoreFile within a Store for each Region for the table)
 

a) Master (HMaster)
================
HMaster is the implementation of the Master Server. 
The Master server is responsible for monitoring all RegionServer instances in the cluster, and is the interface for all metadata changes

master interface

The methods exposed by HMasterInterface are primarily metadata-oriented methods:

Table (createTable, modifyTable, removeTable, enable, disable)
ColumnFamily (addColumn, modifyColumn, removeColumn)
Region (move, assign, unassign)

b) Region (HRegion)
===================
HRegionServer is the RegionServer implementation. 
It is responsible for serving and managing regions

Region Interface
The methods exposed by HRegionRegionInterface contain both data-oriented and region-maintenance methods:

Data (get, put, delete, next, etc.)
Region (splitRegion, compactRegion, etc.)
For example, when the HBaseAdmin method majorCompact is invoked on a table, the client is actually iterating through all regions for the specified table and requesting a major compaction directly to each region.


c) Write Ahead Log (WAL)
========================
Each RegionServer adds updates (Puts, Deletes) to its write-ahead log (WAL) first, and then to the “MemStore” for the “Store”. 
This ensures that HBase has durable writes.
Without WAL, there is the possibility of data loss in the case of a RegionServer failure before each MemStore is flushed and new StoreFiles are written. 
HLog is the HBase WAL implementation, and there is one HLog instance per RegionServer.

The WAL is in HDFS in /hbase/.logs/ with subdirectories per region.